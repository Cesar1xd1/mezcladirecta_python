'''
Created on 1 dic. 2020

@author: Cesar
'''

from array import array
class MetodoOrdenamiento:
    def mezclaDirecta(self,array):
        
        mitad=len(array)//2
        
        if len(array)>=2:
            arregloIz=array[mitad:]
            arregloDer=array[:mitad]

            array.clear()  
            
            self.mezclaDirecta(arregloIz)
            
            self.mezclaDirecta(arregloDer)
            
           
            while(len(arregloDer)>0 and len(arregloIz)>0):
                if(arregloIz[0]< arregloDer[0]):
                    array.append(arregloIz.pop(0))
                else:
                    array.append(arregloDer.pop(0))
           
            while len(arregloIz)>0:
                array.append(arregloIz.pop(0))
            
            while len(arregloDer)>0:
                array.append(arregloDer.pop(0))
        
        return array
    
    def mezclaDirecta2(self, array):
        mitad=len(array)//2
        
        if len(array)>=2:
            arregloIz=array[mitad:]
            arregloDer=array[:mitad]

            array.clear()  
            
            self.mezclaDirecta(arregloIz)
            
            self.mezclaDirecta(arregloDer)
            
           
            while(len(arregloDer)>0 and len(arregloIz)>0):
                if(arregloIz[0]< arregloDer[0]):
                    array.append(arregloIz.pop(0))
                else:
                    array.append(arregloDer.pop(0))
           
            while len(arregloIz)>0:
                array.append(arregloIz.pop(0))
            
            while len(arregloDer)>0:
                array.append(arregloDer.pop(0))
        
        
    def mezclaNatural(self, numeros):
        izquerdo = 0
        izq = 0
        derecho = len(numeros)-1
        der = derecho
        ordenado=False
        
        while(not ordenado):
            ordenado = True
            izquierdo =0
            while(izquierdo<derecho):
                izq=izquerdo
                while(izq < derecho and numeros[izq]<=numeros[izq+1]):
                    izq=izq+1
                der=izq+1
                while(der==derecho-1 or der<derecho and numeros[der]<=numeros[der+1]):
                    der=der+1
                if(der<=derecho):
                    self.mezclaDirecta2(numeros)
                    ordenado= False
                izquierdo = izq
    



numeros = [22,23,30,2,5,10,9,13]
mn = MetodoOrdenamiento()
print(f"Desordenado: {numeros}")
mn.mezclaNatural(numeros)
print(f"Ordenados: {numeros}")